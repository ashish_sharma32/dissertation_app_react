import React, { useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Constants from 'expo-constants';

export default function Post({ navigation }) {
  const [user, setUser] = useState({
    name: '',
    gender: '',
    email: '',
    status: '',
  });

  const [loading, setLoading] = useState(false);

  const onChangeFName = (value) => {
    setUser({ ...user, fname: value });
  };

  const onChangeLName = (value) => {
    setUser({ ...user, lname: value });
  };

  const onChangeEmail = (value) => {
    setUser({ ...user, email: value });
  };

  const onChangeTelephone = (value) => {
    setUser({ ...user, telephone: value });
  };

  const saveData = () => {
    setLoading(true);


    fetch('http://ec2-35-179-12-182.eu-west-2.compute.amazonaws.com/api/entities/store', {
      method: 'POST',
      headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
      body: JSON.stringify({
        fname: user.fname,
        lname: user.lname,
        email: user.email,
        telephone: user.telephone,
      }),
    })
      .then((response) => {
        setLoading(false);
        response.text();
        navigation.push('Home');
      })
      .then((result) => console.log(result))
      .catch((error) => console.log(error));
  };

  return (
    <View style={styles.container}>
      <TextInput
        placeholder={'First Name'}
        onChangeText={(value) => onChangeFName(value)}
        style={styles.input}
      />
      <TextInput
        placeholder={'Last Name'}
        onChangeText={(value) => onChangeLName(value)}
        style={styles.input}
      />
      <TextInput
        placeholder={'Email'}
        onChangeText={(value) => onChangeEmail(value)}
        style={styles.input}
      />
      <TextInput
        placeholder={'Telephone'}
        onChangeText={(value) => onChangeTelephone(value)}
        style={styles.input}
      />

      <TouchableOpacity onPress={saveData}>
        <View style={{ backgroundColor: 'blue', padding: 10 }}>
          <Text style={{ color: 'white', textAlign: 'center' }}>
            {loading ? 'Loading...' : 'Store'}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    padding: 8,
    margin: 15,
  },
  input: {
    height: 40,
    borderWidth: 1,
    borderColor: '#ccc',
    padding: 10,
    marginVertical: 5,
  },
});
