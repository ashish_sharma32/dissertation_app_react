//import liraries
import React, { Component, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';

// create a component
const Detail = ({ route, navigation }) => {
  const { item } = route.params;

  const [user, setUser] = useState({
    fname: item.fname,
    lname: item.lname,
    email: item.email,
    telephone: item.telephone,
  });

  const onChangeFName = (value) => {
    setUser({ ...user, fname: value });
  };

  const onChangeLName = (value) => {
    setUser({ ...user, lname: value });
  };

  const onChangeEmail = (value) => {
    setUser({ ...user, email: value });
  };

  const onChangeTelephone = (value) => {
    setUser({ ...user, telephone: value });
  };

  const updateData = () => {
    fetch(
      'http://ec2-35-179-12-182.eu-west-2.compute.amazonaws.com/api/entities/update/' +
        item.id,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          fname: user.fname,
          lname: user.lname,
          email: user.email,
          telephone: user.telephone,
        }),
      }
    )
      .then((response) => {
        response.text();
        navigation.push('Home');
      })
      .then((result) => console.log(result))
      .catch((error) => console.log(error));
  };

  const deleteData = () => {
    fetch(
      'http://ec2-35-179-12-182.eu-west-2.compute.amazonaws.com/api/entities/delete/' +
        item.id,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        }
      }
    )
      .then((response) => {
        response.text();
        navigation.push('Home');
      })
      .then((result) => console.log(result))
      .catch((error) => console.log(error));
  };

  return (
    <View style={styles.container}>
      <TextInput
        placeholder={'First Name'}
        onChangeText={(value) => onChangeFName(value)}
        style={styles.input}
        value={user.fname}
      />
      <TextInput
        placeholder={'Last Name'}
        onChangeText={(value) => onChangeLName(value)}
        style={styles.input}
        value={user.lname}
      />
      <TextInput
        placeholder={'Email'}
        onChangeText={(value) => onChangeEmail(value)}
        style={styles.input}
        value={user.email}
      />
      <TextInput
        placeholder={'Phone'}
        onChangeText={(value) => onChangeTelephone(value)}
        style={styles.input}
        value={user.telephone}
      />

      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity onPress={updateData}>
          <View style={{ backgroundColor: 'blue', padding: 10 }}>
            <Text style={{ color: 'white', textAlign: 'center' }}>Update</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={deleteData}>
          <View style={{ backgroundColor: 'red', padding: 10 }}>
            <Text style={{ color: 'white', textAlign: 'center' }}>Delete</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 15,
    backgroundColor: '#fff',
  },
  input: {
    height: 40,
    borderWidth: 1,
    borderColor: '#ccc',
    padding: 10,
    marginVertical: 5,
  },
});

//make this component available to the app
export default Detail;
